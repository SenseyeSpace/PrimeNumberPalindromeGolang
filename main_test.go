package main

import "testing"

func TestIsPalindrome(t *testing.T) {
	assertSame(t, true, isPalindrome(987656789))

	assertSame(t, false, isPalindrome(987654321))
}

func assertSame(t testing.TB, expect, value bool) {
	if expect != value {
		t.Error("expect same")
	}
}
