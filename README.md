# Palindrome made from the product of two 5-digit prime numbers

##### Result
```
Palindrome: 999949999
Prime numbers 30109 * 33211
Duration: 19 623 533
```

##### Optimize:
* Ознака ділення на 11 — *Число ділиться на 11, якщо сума цифр на парних місцях мінус сума на непарних ділиться на 11*

##### Languages:
* [JavaScript](https://gitlab.com/Senseye/PrimeNumberPalindromeJavaScript)
* [Python](https://gitlab.com/Senseye/PrimeNumberPalindromePython)
* [C](https://gitlab.com/Senseye/PrimeNumberPalindromeC)
* [Rust](https://gitlab.com/Senseye/PrimeNumberPalindromeRust)